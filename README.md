# docker_wrapper
Wrapper script and extra commands for Docker

The main script here is `docker` which should be called instead of the
Docker binary. It will look at the Docker subcommand to determine if there
is a support script (named `docker-<subcommand>`)  that should be executed.

## Docker subcommand interface

The subcommand scripts can be written in nearly any language (script or
compiled) and just needs to be in the executable path of the current shell.
When the script starts it's command line will be set to the subcommand
script name followed by any of the arguments that were passed on the original
command line.

In the script's execution environment, the variable `DOCKER_EXEC` will be
set to the command to execute for the Docker CLI. Originally this would
be set to just the `docker` binary, but with the current support of multiple
Docker engines (`podman` and `nerdctl` under lima) the contents may be a
partial command line to get the engine running.

## Docker subcommands

Some of these subcommands were written before the support for multiple
engines was implemented and have not been updated to support more than
just Docker Engine.

- `docker-cleanup`

    This does a simple cleanup of the completed containers and removes
    orphaned container images.

- `docker-get`

    Perform a `docker pull` of the referenced image and then retag the image
    with the basename/tag of the image
    (i.e. registry.gitlab.com/wt0f/docker_terraform:latest becomes
    docker_terraform:latest). This is useful for containers that are used
    frequently for testing or development without having to specify the
    fully qualified container name.

- `docker-list`

- `docker-lslabels`

    Inspect the specified container image and list all the labels that
    have been added to the image.

- `docker-lstags`

- `docker-rmtag`

    Remove all container images with the specified tag(s). This script
    can also be symbolicly linked to `docker-rmtags` if one desires to
    have the `rmtags` subcommand.

- `docker-setengine`

    Set the Docker engine in use rather than have the `docker` wrapper
    determine at runtime which engine to use. This has a slight performance
    advantage (not much given the performance of current hardware), but
    can be useful if there are multiple Docker engines installed and there
    is a preference as to which one to execute.